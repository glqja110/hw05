#include "message_book.h"

using namespace std;



int main()
{
	MessageBook MSG; 
	while(true) {
	string inputs;   //inputs라는 입력 하는 string 선언.
	cin>>inputs;

	if(inputs == "quit") return false; //#quit 입력하면 탈출.
  	else if(inputs=="add") {      //add _NUM _NAME 
		string _NUM;   
		string _NAME;
        	cin>>_NUM;
		getline(cin,_NAME);
		_NAME.erase(0,1);
        	int number=-1;
        	for(int i=0; i<_NUM.size(); ++i) {
            		int num = (int)_NUM[i] - (int)'0';  
            		if(number < 0) number = num; 
            		else number = number * 10 + num; 
		}	
		//number와 _NAME을 정했음.
		MSG.AddMessage(number,_NAME);
	}

	else if(inputs=="delete") {  //delete D_NUM.
        	string _NUM;    
        	cin>>_NUM;
        	int number=-1;
        	for(int i=0; i<_NUM.size(); ++i) {
            		int num = (int)_NUM[i] - (int)'0';  
            		if(number < 0) number = num; 
            		else number = number * 10 + num; 
		}
		//지울 숫자 부분인 number를 정함
		MSG.DeleteMessage(number);
	}

	else if(inputs=="print") {  //print P_NUM
        	string _NUM;   
        	cin>>_NUM;
        	int number=-1;
        	for(int i=0; i<_NUM.size(); ++i) {
            		int num = (int)_NUM[i] - (int)'0';  
            		if(number < 0) number = num; 
            		else number = number * 10 + num; 
		}
		//출력할 숫자 부분인 number를 정함
		MSG.GetMessage(number);
	}

	else if(inputs=="list") 
		MSG.List();
    	}
    	return 0;
}

