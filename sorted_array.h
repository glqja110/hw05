#ifndef sorted_array_h
#define sorted_array_h

#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <map>
#define MAX_SIZE 100


using namespace std;

class SortedArray {
 public:
  SortedArray();
  ~SortedArray();

  void AddNumber(int num);

  void GetSortedAscending();             //ascend
  void GetSortedDescending();          //descend
  int GetMax();              //max
  int GetMin();              //min
  int ArrayNumbers() const;     

 private:
  vector<int> numbers_;
  int Count;
};

#endif /* sorted_array_h */