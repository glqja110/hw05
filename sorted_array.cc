#include <iostream>
#include <stdlib.h>
#include <set>
#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#define MAX_SIZE 100
#include "sorted_array.h"

using namespace std;
//numbers_벡터 초기화
SortedArray::SortedArray() {
	numbers_.reserve(MAX_SIZE);
	Count=0;
}

SortedArray::~SortedArray() { }

int SortedArray::ArrayNumbers() const {
	return Count;
}
//입력한 수들을 numbers_에 추가하는 과정.
void SortedArray::AddNumber(int num) {
	numbers_[Count]=num;
	Count++;
}

//ascend라 쳤을 때 numbers_벡터 안에 들어가있던 수들을 순서대로 정렬하는 과정.
void SortedArray::GetSortedAscending() {
	int tmp;
	for(int i=0;i<Count-1;i++) {
		for(int j=0;j<Count-1;j++) {
			if(numbers_[j]>numbers_[j+1]) {
				tmp=numbers_[j];
				numbers_[j]=numbers_[j+1];
				numbers_[j+1]=tmp;
			}
		}
	}
	for(int i=0;i<Count;i++)
		cout<<numbers_[i]<<' ';
	cout<<endl;
}
//descend라 쳤을때 이번엔 오름 차순으로 정렬하는 과정.
void SortedArray::GetSortedDescending() {
	int tmp;
	for(int i=0;i<Count-1;i++) {
		for(int j=0;j<Count-1;j++) {
			if(numbers_[j]<numbers_[j+1]) {
				tmp=numbers_[j];
				numbers_[j]=numbers_[j+1];
				numbers_[j+1]=tmp;
			}
		}
	}
	for(int i=0;i<Count;i++)
		cout<<numbers_[i]<<' ';
	cout<<endl;
}

//가장 큰 수를 골라내는 과정
int SortedArray::GetMax() {
	int tmp;
	for(int i=0;i<Count-1;i++) {
		for(int j=0;j<Count-1;j++) {
			if(numbers_[j]>numbers_[j+1]) {
				tmp=numbers_[j];
				numbers_[j]=numbers_[j+1];
				numbers_[j+1]=tmp;
			}
		}
	}
	return numbers_[Count-1];
}
// 가장 작은 수를 골라내는 과정
int SortedArray::GetMin() {
	int tmp;
	for(int i=0;i<Count-1;i++) {
		for(int j=0;j<Count-1;j++) {
			if(numbers_[j]<numbers_[j+1]) {
				tmp=numbers_[j];
				numbers_[j]=numbers_[j+1];
				numbers_[j+1]=tmp;
			}
		}
	}
	return numbers_[Count-1];
}