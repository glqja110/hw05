#include <iostream>
#include <stdlib.h>
#include <set>
#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#define MAX_SIZE 1024 
#include "simple_int_set.h"

using namespace std;

int SimpleIntSet::elementCount() const
{
	return mElements.size();
}

SimpleIntSet::SimpleIntSet() {
	mElementCount=0;
}



SimpleIntSet::~SimpleIntSet() {}

SimpleIntSet::SimpleIntSet(int *_elements, int _count) //생성자
{
	mElementCount=_count;
	for(int i=0;i<mElementCount;i++)
		mElements.insert(_elements[i]);

	mElementss=new int[mElements.size()];
	int k=0;
	for(it=mElements.begin();it!=mElements.end();it++) {
		mElementss[k]=*it;
		k++;
	}

}

SimpleIntSet* SimpleIntSet::unionSet(SimpleIntSet& _operand)
{
	//right 숫자 중에서 left 숫자랑 같은게 아무것도 없으면 left의 새로운 원소가 되는 과정.
	int check=0; //right 숫자 중에서 left 숫자랑 같은게 아무것도 없는지 체크하는 변수. 
	for(int i=0; i<(_operand.mElementCount) ;i++)
	{
		check=0;
		for(int j=0; j<mElementCount;j++)
		{
			if((_operand.mElementss[i])==mElementss[j])
				check+=1;
		}
		if(check==0)
		{
			mElements.insert(_operand.mElementss[i]);
		}
	}
}

SimpleIntSet* SimpleIntSet::differenceSet(SimpleIntSet& _operand)
{
	//같은 숫자 있으면 빼내버리는 과정.
	for(int i=0; i<mElementCount ;i++)
	{
		for(int j=0; j<(_operand.mElementCount);j++)
		{
			if(mElementss[i]==(_operand.mElementss[j]))
			{
				it=mElements.find((_operand.mElementss[j]));
				mElements.erase(it);
			}
		}
	}
	
		
}

SimpleIntSet* SimpleIntSet::intersectSet(SimpleIntSet& _operand)
{
	//operand와 비교해서 교차되는 것이 없는것을 찾아내는 과정.
	int i,j,tmp;
	int check=0;;
	for(i=0; i<mElementCount ;i++)
	{
		check=0;
		for(j=0; j<(_operand.mElementCount);j++)
		{
			if(mElementss[i]!=(_operand.mElementss[j]))
			{
				check+=1;
			}
				
		}
		if(check==(_operand.mElementCount))
		{
			it=mElements.find(mElementss[i]);
			mElements.erase(it);
		}
	}
	
}

void SimpleIntSet::printSet()
{
	int i;
	cout<<"{ ";
	for (it=mElements.begin();it!=mElements.end();it++)
		cout<<*it<<' ';
	cout<<"}"<<endl;
}
