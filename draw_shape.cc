#include "draw_shape.h"

using namespace std;


Canvas::Canvas() {
	row_=0;
	col_=0;
	order=0;
}

Canvas::~Canvas() {
	
}

void Canvas::setCanvas(size_t row, size_t col) {
	row_=row;
	col_=col;
	shapes_.reserve(row_);
	for(int i=0; i<row_;i++)                //canvas를 입력한 크기만큼 정해줌
		shapes_[i].resize(col_,"."); 
}

int Canvas::AddShape(const Shape &s) {
	if(s.type==1) {
		if(s.x<0||s.y<0||s.x>=col_||s.y>=row_||s.x-((s.width-1)/2)<0||s.x+((s.width-1)/2)>col_||s.y-((s.height-1)/2)<0||s.y+((s.height-1)/2)>row_)     //범위를 벗어나는 입력
			return ERROR_OUT_OF_CANVAS;
		else if(s.width%2==0||s.height%2==0)     //짝수 입력
			return ERROR_INVALID_INPUT;
		else {
			string lister;                  //여기에 dump 치면 출력될 것들이 입력됨.
			lister+="rect ";
			lister+=((char)s.x+(char)'0');
			lister+=' ';
			lister+=((char)s.y+(char)'0');
			lister+=' ';
			lister+=((char)s.width+(char)'0');
			lister+=' ';
			lister+=((char)s.height+(char)'0');
			lister+=' ';
			lister+=s.brush;
			list.insert(make_pair(order,lister));
			order++;
			//해당 구역을 s.brush로 바꿔주는 작업.
			for(int i=s.y-(s.height-1)/2;i<=s.y+(s.height-1)/2;i++) {
				for(int j=s.x-(s.width-1)/2;j<=s.x+(s.width-1)/2;j++) 
					shapes_[i][j]=s.brush;
			}
		}
		return 1;
	}

	else if(s.type==2) {
		if(s.x<0||s.y<0||s.x>=col_||s.y>=row_||s.x-(s.height-1)<0||s.x+(s.height-1)>=col_||s.y+(s.height-1)>=row_)
			return ERROR_OUT_OF_CANVAS;
		else {
			string lister;
			lister+="tri_up ";
			lister+=((char)s.x+(char)'0');
			lister+=' ';
			lister+=((char)s.y+(char)'0');
			lister+=' ';
			lister+=((char)s.height+(char)'0');
			lister+=' ';
			lister+=s.brush;
			list.insert(make_pair(order,lister));
			order++;
			int side=0;
			//해당 구역을 s.brush로 바꿔주는 작업.
			for(int i=s.y;i<(s.y+s.height);i++) {
				for(int j=(s.x-side);j<=(s.x+side);j++) {
					shapes_[i][j]=s.brush;
				}
				side++;
			}
		}
		return 1;
			
	}

	else if(s.type==3) {
		if(s.x<0||s.y<0||s.x>=col_||s.y>=row_||s.x-(s.height-1)<0||s.x+(s.height-1)>=col_||s.y-(s.height-1)<0)
			return ERROR_OUT_OF_CANVAS;
		else {
			string lister;
			lister+="tri_down ";
			lister+=((char)s.x+(char)'0');
			lister+=' ';
			lister+=((char)s.y+(char)'0');
			lister+=' ';
			lister+=((char)s.height+(char)'0');
			lister+=' ';
			lister+=s.brush;
			list.insert(make_pair(order,lister));
			order++;
			int side=0;
			//해당 구역을 s.brush로 바꿔주는 작업.
			for(int i=s.y;i>s.y-s.height;i--) {
				for(int j=s.x-side;j<=s.x+side;j++) {
					shapes_[i][j]=s.brush;
					
				}
				side++;
			}
		}
		return 1;
	}
}

void Canvas::DeleteShape(int index) {
	int check_=-1;
	map<int,string>::iterator it;
	for(it=list.begin();it!=list.end();it++) {                //delete 숫자 쳤는데 숫자라고 입력된것이 있는지 없는지 확인
		if(it->first==index) {
			check_=1;
			break;
		}
	}
	if(check_==1) {

	string select=it->second;
	string types;
	for(int i=0;i<select.size();i++) {
		if(select[i]!=' ')
			types+=select[i];
		else if(select[i]==' ') 
			break;
	}
	for(it=list.begin();it!=list.end();it++) {
		if(it->first==index)
			break;
	}
	list.erase(it);	
	if(list.size()>0) {
		int set;
		string set2;
		if(list.size()>0) {
			for(it=list.find(index+1);it!=list.end();it++) {
				set=it->first;
				set2=it->second;
				set--;
				list.erase(it);
				list.insert(make_pair(set,set2));
			}
		}	
	}
	//숫자 입력된 부분 지워내고 그 뒷부분에 있는 숫자들 앞으로 땡겨옴.
	if(types=="rect") {
		select.erase(0,5);
		int number=-1;
		int x_=-1;
		int y_=-1;
		int wid_=-1;
		int hei_=-1;
		char bru_;
		for(int i=0; i<select.size(); ++i) {
            		char c = select[i];
            
            		if(isdigit(c) == true)
            		{
                		int num = (int)c - (int)'0'; 
                		if(number < 0) number = num; 
                		else number = number * 10 + num; 
            		}
            		else
            		{
                		if(c == ' ') {
					if(x_<0)
						x_=number;
					else if(y_<0)
						y_=number;
					else if(wid_<0)
						wid_=number;
					else if(hei_<0)
						hei_=number;
				}
                		else 
					bru_=c;
                		number = -1;
            		}
        	}	
		//x y width height brush들을 다 저장시키고 이제 그걸 토대로 그 해당 부분을 지워 낼 것임.
		for(int i=y_-(hei_-1)/2;i<=y_+(hei_-1)/2;i++) {
			for(int j=x_-(wid_-1)/2;j<=x_+(wid_-1)/2;j++) 
				shapes_[i][j]=".";
		}
		
	}

	else if(types=="tri_up") {
		select.erase(0,7);
		int number=-1;
		int x_=-1;
		int y_=-1;
		int hei_=-1;
		char bru_;
		for(int i=0; i<select.size(); ++i) {
            		char c = select[i];
            
            		if(isdigit(c) == true)
            		{
                		int num = (int)c - (int)'0'; 
                		if(number < 0) number = num; 
                		else number = number * 10 + num; 
            		}
            		else
            		{
                		if(c == ' ') {
					if(x_<0)
						x_=number;
					else if(y_<0)
						y_=number;
					else if(hei_<0)
						hei_=number;
				}
                		else 
					bru_=c;
                		number = -1;
            		}
        	}	
		//x y height brush들을 다 저장시키고 이제 그걸 토대로 그 해당 부분을 지워 낼 것임.
		int side=0;
		for(int i=y_;i<y_+hei_;i++) {
			for(int j=x_-side;j<=x_+side;j++) {
				shapes_[i][j]=".";
				
			}
			side++;
		}
		order--;
	}

	else if(types=="tri_down") {
		select.erase(0,9);
		int number=-1;
		int x_=-1;
		int y_=-1;
		int hei_=-1;
		char bru_;
		for(int i=0; i<select.size(); ++i) {
            		char c = select[i];
            
            		if(isdigit(c) == true)
            		{
                		int num = (int)c - (int)'0'; 
                		if(number < 0) number = num; 
                		else number = number * 10 + num; 
            		}
            		else
            		{
                		if(c == ' ') {
					if(x_<0)
						x_=number;
					else if(y_<0)
						y_=number;
					else if(hei_<0)
						hei_=number;
				}
                		else 
					bru_=c;
                		number = -1;
            		}
        	}	
		//x y height brush들을 다 저장시키고 이제 그걸 토대로 그 해당 부분을 지워 낼 것임.
		int side=0;
		for(int i=y_;i>y_-hei_;i--) {
			for(int j=x_-side;j<=x_+side;j++) {
				shapes_[i][j]=".";
				
			}
			side++;
		}
		order--;
	}
	}

}

void Canvas::Draw(ostream& os) {
	os<<" ";
	for(int i=0;i<col_;i++) {
		if(col_<10)
		os<<i;
		else
			os<<(i%10);
	}
	os<<endl;
	for(int i=0;i<row_;i++) {
		os<<i;
		for(int j=0;j<col_;j++)
			os<<shapes_[i][j];
		os<<endl;
	}	
}

void Canvas::Dump(ostream& os) {
	map<int,string>::iterator it;
	for(it=list.begin();it!=list.end();it++)
		os<<it->first<<" "<<it->second<<endl;
}