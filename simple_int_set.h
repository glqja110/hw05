// simple_int_set.h
// Implement your simple_int_set.cc

#ifndef simple_int_set
#define simple_int_set

#include <iostream>
#include <stdlib.h>
#include <set>
#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#define MAX_SIZE 1024 

using namespace std;

class SimpleIntSet
{
private: 
	int *mElementss;
    	set<int> mElements;     //int형 배열을 int형 set으로 대체할것임.
	set<int>::iterator it;     //iterator 쓸것들 여기다가 추가.
	int mElementCount;     

	//void sortElements(); // you can reuse your previous sort assignment  test해보니 알아서 정렬 되었음.
	SimpleIntSet();
    
public:  //모든 public 함수 구현해야함.
	SimpleIntSet(int *_elements, int _count); //생성자     
	~SimpleIntSet(); //소멸자
    
	int elementCount() const; 
    	
	SimpleIntSet *unionSet(SimpleIntSet& _operand);
	SimpleIntSet *differenceSet(SimpleIntSet& _operand);
	SimpleIntSet *intersectSet(SimpleIntSet& _operand);
   

	void printSet();    //출력하는 함수.
};

#endif

