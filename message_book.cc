#include "message_book.h"

using namespace std;

MessageBook::MessageBook() {}

MessageBook::~MessageBook() {}

void MessageBook::AddMessage(int number, const string& message) {
	int check=0; //add에 있는 숫자가 맵에 있는 숫자면 1됨.
	for(it=messages_.begin();it!=messages_.end();it++) {
		if(number==(it->first))
			check=1;
	}
	
	if(check==1) {
		it=messages_.find(number);
		messages_.erase(it);
		messages_.insert(make_pair(number,message));
	}
	else	
		messages_.insert(make_pair(number,message));
}

void MessageBook::DeleteMessage(int number) {
	int check=0; 	//delete에 있는 숫자가 맵에 있는 숫자면 1됨.
	for(it=messages_.begin();it!=messages_.end();it++) {
		if(number==(it->first))
			check=1;
	}
	if(check==1) {
		it=messages_.find(number);
		messages_.erase(it);
	}
}



void MessageBook::GetMessage(int number) {
	int check=0;  //print에 있는 숫자가 맵에 있는 숫자면 1됨.	
	for(it=messages_.begin();it!=messages_.end();it++) {
		if(number==(it->first))
			check=1;
	}
	if(check==1){
		it=messages_.find(number);
		cout<<(it->second)<<endl;
	}
}

void MessageBook::List() {
	for(it=messages_.begin();it!=messages_.end();it++)
		cout<<(it->first)<<": "<<(it->second)<<endl;	
}

