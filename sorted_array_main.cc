#include <iostream>
#include <stdlib.h>
#include <set>
#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#define MAX_SIZE 100
#include "sorted_array.h"

using namespace std;



bool creative(SortedArray* ARY)
{
	string inputs;  //inputs라는 입력 하는 string 선언.
  	getline(cin, inputs);

	if(inputs == "quit") return false;  //#quit 입력하면 탈출.
  	else if(inputs.find("ascend") != string::npos) {     //add _NUM _NAME 
		ARY->GetSortedAscending();
	}

	else if(inputs.find("descend") != string::npos) {   //delete D_NUM.
        	ARY->GetSortedDescending();
	}

	else if(inputs.find("max") != string::npos) {  //print P_NUM
        	cout<<ARY->GetMax()<<endl;
	}

	else if(inputs.find("min") != string::npos) {  //:Burn # or :Burn name #
		cout<<ARY->GetMin()<<endl;
	}
	else {
		int*NUMBERS=new int[MAX_SIZE];	
		int number = -1;
		int indi=0;
		for(int i=0; i<inputs.size(); ++i) {
			char c = inputs[i];
			if(isdigit(c) == true) {
            			int num = (int)c - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
            			if(number < 0) number = num; 
            			else number = number * 10 + num;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
			}
			else {
				if(c=' ') {
					NUMBERS[indi]=number;
					number=-1;
					indi++;
				}
			}
           	}
		NUMBERS[indi]=number;  	
		indi++;
		//indi가 NUMBERS에 들어있는 숫자들의 갯수. 
		for(int i=0;i<indi;i++) {
			ARY->AddNumber(NUMBERS[i]);	
		}
	
		
	}
    
    	return true; // 이상한거 입력 안되면 true 나오게 되어있음.
}

int main()
{	
	SortedArray* arr=new SortedArray();     
	while(creative(arr)){ }
	delete arr;
	return 0;
}
		
