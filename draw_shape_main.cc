#include "draw_shape.h"
using namespace std;

int main()
{
	Canvas CAN;
	while(true){
		string inputs;  //inputs라는 입력 하는 string 선언.
  		cin>>inputs;

		if(inputs == "quit") return false;  //#quit 입력하면 탈출.

  		else if(inputs=="add") {     //add 도형종류 숫자들.
        		string kind;      //도형의 종류를 입력
			cin>>kind;		
			if(kind=="rect") {      //rect R C 너비 높이  도형
			int _R,_C,R_X,R_Y;
			char cha;
			cin>>_C>>_R>>R_X>>R_Y>>cha;
			//_R _C R_X R_Y cha 가 다 정해짐.
			Shape rect;
			rect.type=1;      //rect 타입은 1
			rect.x=_C;
			rect.y=_R;
			rect.width=R_X;
			rect.height=R_Y;
			rect.brush=cha;
			int check=CAN.AddShape(rect);
			if(check==-1)
				cout<<"error out of canvas"<<endl;
			else if(check==-2)
				cout<<"error invalid input"<<endl;			
			}

			else if(kind=="tri_up") {      //tri_up X Y 높이  도형
			int T_X,T_Y,T_H;
			char cha;	
			cin>>T_X>>T_Y>>T_H>>cha;
			//T_X, T_Y, T_H, cha 가 다 정해짐.
			Shape tri_up;
			tri_up.type=2;      //tri_up 타입은 2
			tri_up.x=T_X;
			tri_up.y=T_Y;
			tri_up.height=T_H;
			tri_up.brush=cha;
			int check=CAN.AddShape(tri_up);
			if(check==-1)
				cout<<"error out of canvas"<<endl;			
			}

			else if(kind=="tri_down") {       //tri_down X Y 높이 도형
			int T_X,T_Y,T_H;
			char cha;	
			cin>>T_X>>T_Y>>T_H>>cha;
			//T_X, T_Y, T_H, cha 가 다 정해짐.
			Shape tri_down;
			tri_down.type=3;      //tri_down 타입은 3
			tri_down.x=T_X;
			tri_down.y=T_Y;
			tri_down.height=T_H;
			tri_down.brush=cha;
			int check=CAN.AddShape(tri_down);
			if(check==-1)
				cout<<"error out of canvas"<<endl;
			}
        	}
		else if(inputs=="draw") 
			CAN.Draw(cout);

		else if(inputs=="dump")
			CAN.Dump(cout);

		else if(inputs=="delete") {
			int d_num;
			cin>>d_num;
			CAN.DeleteShape(d_num);
		}

		else {                             //캔버스 크기 정하기.
		int CAN_R=-1;
		int CAN_C;                              
		cin>>CAN_C;
		for(int i=0;i<inputs.size();i++) {
		int num = (int)(inputs[i]) - (int)'0'; 
                	if(CAN_R < 0) 
			CAN_R = num; 
                	else 
			CAN_R = CAN_R * 10 + num;
		}  
		CAN.setCanvas(CAN_R,CAN_C);
		CAN.Draw(cout);
		}
	}
    
	return 0;
}
		
