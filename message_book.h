#ifndef message_book_h
#define message_book_h

#include <string>
#include <vector>
#include <iostream>
#include <map>

using namespace std;

class MessageBook {
 public:
  MessageBook();
  ~MessageBook();
  void AddMessage(int number, const string& message);      //add 숫자 "문자" 이렇게 입력할텐데 그게 messages_에 입력되게 만드는 함수.
  void DeleteMessage(int number);    //delete 숫자 이렇게 입력할 텐데 messages_에 해당되는 숫자가 삭제되게 만드는 함수.
  void GetMessage(int number);    //문자 부분을 리턴하는 함수 같아보임.
  void List();

 private:
  map<int, string> messages_;
  map<int, string>::iterator it; 
};

#endif /* message_book_h */